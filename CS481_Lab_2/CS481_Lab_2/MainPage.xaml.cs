﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace CS481_Lab_2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
       // first commit
       // second commit
        public ObservableCollection<ListViewItems> Items { get; set; } 
        
        public MainPage()
        {
            InitializeComponent();
            List();
            
        }

        void List()
        {
            Items = new ObservableCollection<ListViewItems>();

            // Creating list items
            Items.Add(new ListViewItems
            {
                Name = "United States",
                Image_1 = "https://cdn.britannica.com/33/4833-050-0FE70191/Flag-United-States-of-America.jpg",
                Image_2 = "https://images-na.ssl-images-amazon.com/images/I/81r8Hxz5lQL._AC_SL1500_.jpg",
                
                Text = "Welcome to United States!"
            }) ;
            Items.Add(new ListViewItems
            {
                Name = "Mexico",
                Image_1 = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/1200px-Flag_of_Mexico.svg.png",
                Image_2 = "https://images-na.ssl-images-amazon.com/images/I/71UbYF1fX4L._AC_SL1500_.jpg",
                
                Text = "Welcome to Mexico"
            });

            Items.Add(new ListViewItems
            {
                Name = "South Korea",
                Image_1 = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRoeGgEg-U1asBWM2wcc9Mbxie-LhJEKkN01v3JFn1KCFaqKISQ",
                Image_2 = "https://www.mapsofworld.com/south-korea/maps/south-korea-map.gif",
              
                Text = "Welcome to South Korea"
            });

            Items.Add(new ListViewItems
            {
                Name = "United Kingdom",
                Image_1 = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Flag_of_Great_Britain_%281707%E2%80%931800%29.svg/1024px-Flag_of_Great_Britain_%281707%E2%80%931800%29.svg.png",
                Image_2 = "https://geology.com/world/united-kingdom-map.gif",
            
                Text = "Welcome to United Kingdom"
            });

            Example.ItemsSource = Items;
            Example.ItemTapped += Example_ItemTapped;
        }

        public void AddItems()
        {
            Items.Add(new ListViewItems { Name = "Name", Text ="Text" });
          
            
        }

        private async void Example_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            // More details about one item
            var detail = e.Item as ListViewItems;
            await Navigation.PushAsync(new Page1(detail.Name, detail.Text, detail.Image_2));
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            // Refresh the list
            List();
            Example.IsRefreshing = false;
        }

        void Handle_Delete(object sender, System.EventArgs e)
        {

            //Delete Item
            var menuItem = (MenuItem)sender;
            var DeleteItems = (ListViewItems)menuItem.CommandParameter;

            DisplayAlert("Delete Items", menuItem.CommandParameter + " was deleted", "OK");
            Items.Remove(DeleteItems);


        }

        public async void Handle_Visit(object sender, System.EventArgs e)
        {
            var mi = (MenuItem)sender;
            ListViewItems item = (ListViewItems)mi.CommandParameter;

           
          
            if(item.Name == "United States")
            
            {
                await Navigation.PushAsync(new United_State());   

            }

            else if(item.Name == "Mexico")
            {
                await Navigation.PushAsync(new Mexico());
            }
            else if(item.Name == "South Korea")
            {
                await Navigation.PushAsync(new Sourth_Korea());
            }

            else if(item.Name == "United Kingdom")
            {
                await Navigation.PushAsync(new United_State());
            }
           
  

        }
    }
}
