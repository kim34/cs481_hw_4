﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Lab_2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Sourth_Korea : ContentPage
    {
        public Sourth_Korea()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}